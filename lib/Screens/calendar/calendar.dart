import 'package:flutter/material.dart';
import 'package:proces/Screens/Home/home.dart';

import 'package:syncfusion_flutter_calendar/calendar.dart';

class Calendar extends StatefulWidget {
  const Calendar({Key? key}) : super(key: key);

  @override
  _CalendarState createState() => _CalendarState();
}

class _CalendarState extends State<Calendar> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //appBar: AppBar(
      //title: const Text("Calendar"),
      //backgroundColor: Colors.blueAccent,

      //buildCalendar();
      //),
      body: Container(
        child: SfCalendar(),
      ),
    );
  }
/*
 @override
  Widget build(BuildContext context) {
  return Scaffold(
      body: SfCalendar(
    view: CalendarView.month,
  ));
}
*/
}

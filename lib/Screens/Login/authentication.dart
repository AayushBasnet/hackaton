import 'dart:html';

/*
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthenticationService extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class AuthenticationServices {
  late final FirebaseAuth _firebaseAuth;
  //String signIn=''; 
  AuthenticationServices(this._firebaseAuth);

  Future <String> signIn({String email, String password})
  try{
  await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
  
  } on FirebaseAuthException catch (e) {
    return e.message;
}

}





//////
abstract class BaseAuth {
  Future<String> signIn(String email, String password);

  Future<String> signUp(String email, String password);

  Future<User> getCurrentUser();

  Future<void> sendEmailVerification();

  Future<void> signOut();

  Future<bool> isEmailVerified();

  Future<void> sendPasswordReset(String email);
}

class Auth implements BaseAuth {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  Future<String> signIn(String email, String password) async {
    User user = await _firebaseAuth.signInWithEmailAndPassword(
        email: email, password: password);
    return user.uid;
  }

  

  Future<User> getCurrentUser() async {
    User user = await _firebaseAuth.currentUser();
    return user;
  }

  Future<void> signOut() async {
    return _firebaseAuth.signOut();
  }

  Future<void> sendEmailVerification() async {
    User user = await _firebaseAuth.currentUser();
    user.sendEmailVerification();
  }

  Future<bool> isEmailVerified() async {
    User user = await _firebaseAuth.currentUser();
    return user.isEmailVerified;
  }

  Future<void> sendPasswordReset(String email) async {
    _firebaseAuth.sendPasswordResetEmail(email: email);
  }
}
*/

import 'package:firebase_auth/firebase_auth.dart';
import 'dart:async';
//import 'package:google_sign_in/google_sign_in.dart';
/*
class FirebaseAuthentication {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  //final GoogleSignIn googleSignIn = GoogleSignIn();

  Future<String> createUser(String email, String password) async {
    try {
      UserCredential credential = await _firebaseAuth
          .createUserWithEmailAndPassword(email: email, password: password);
      return credential.user!.uid;
    } on FirebaseAuthException {
      return null!;
    }
  }

  Future<String> login(String email, String password) async {
    try {
      UserCredential credential = await _firebaseAuth
          .signInWithEmailAndPassword(email: email, password: password);
      return credential.user!.uid;
    } on FirebaseAuthException {
      return null!;
    }
  }

  Future<bool> logout() async {
    try {
      _firebaseAuth.signOut();
      return true;
    } on FirebaseAuthException {
      return false;
    }
  }
}
*/
/*
  Future<String> loginWithGoogle() async {
    //final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;
    final AuthCredential authCredential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );
    final UserCredential authResult =
        await _firebaseAuth.signInWithCredential(authCredential);
    final User user = authResult.user;
    if (user != null) {
      return '$user';
    }
    return null;
  }
}
*/

class auth_control {
  //static auth_control instance = Get.find();
  FirebaseAuth auth = FirebaseAuth.instance;
  String? errorMessage;

  Future<void> signIn(String email, String password) async {
    try {
      await auth
          .signInWithEmailAndPassword(email: email, password: password)
          .then((uid) => {
                //NavigationService().navigateToScreen(const admissionpage())
              });
    } on FirebaseAuthException catch (error) {
      switch (error.code) {
        case "invalid-email":
          errorMessage = "Your email address appears to be malformed.";
          print(errorMessage);
          break;
        case "wrong-password":
          errorMessage = "Your password is wrong.";
          print(errorMessage);
          break;
        case "user-not-found":
          errorMessage = "User with this email doesn't exist.";
          print(errorMessage);
          break;
        case "user-disabled":
          errorMessage = "User with this email has been disabled.";
          print(errorMessage);
          break;
        case "too-many-requests":
          errorMessage = "Too many requests";
          print(errorMessage);
          break;
        case "operation-not-allowed":
          errorMessage = "Signing in with Email and Password is not enabled.";
          print(errorMessage);
          break;
        default:
          errorMessage = "An undefined Error happened.";
          print(errorMessage);
      }
      //Fluttertoast.showToast(msg: errorMessage!);
      print(error.code);
    }
  }
}

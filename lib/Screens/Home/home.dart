import 'package:flutter/material.dart';
import 'package:proces/Screens/Home/homepage.dart';
import 'package:proces/Screens/calendar/calendar.dart';
import '../mind_mapping/mindMapping.dart';
import 'package:proces/Screens/notifications/notifications.dart';
import 'package:proces/Screens/processess/process.dart';
import 'package:proces/Screens/tasks/tasks.dart';
import 'package:proces/Screens/user/user_profile.dart';
import 'package:proces/Screens/user_stories/userStories.dart';

class home extends StatefulWidget {
  const home({Key? key}) : super(key: key);

  @override
  _homeState createState() => _homeState();
}

class _homeState extends State<home> {
  int _currentIndex = 1;
  final padding = EdgeInsets.symmetric(horizontal: 20);
  final screens = [
    const Calendar(),
    const homepage(),
    const notification(),
  ];
  @override
  Widget build(BuildContext context) {
    //return Drawer();
    return Scaffold(
      drawer: Drawer(
          child: Material(
        color: Colors.blueGrey,
        child: ListView(
          children: <Widget>[
            const SizedBox(height: 48),
            buildMenuItem(
              text: "Home",
              icon: Icons.home,
              onClicked: () => selectedItem(context, 0),
            ),
            const SizedBox(height: 20),
            buildMenuItem(
              text: "Processes",
              icon: Icons.article_rounded,
              onClicked: () => selectedItem(context, 1),
            ),
            const SizedBox(height: 20),
            buildMenuItem(
              text: "Mind-mapping",
              icon: Icons.category_rounded,
              onClicked: () => selectedItem(context, 2),
            ),
            const SizedBox(height: 20),
            buildMenuItem(
              text: "Tasks",
              icon: Icons.task,
              onClicked: () => selectedItem(context, 3),
            ),
            const SizedBox(height: 20),
            buildMenuItem(
              text: "User-stories",
              icon: Icons.amp_stories_rounded,
              onClicked: () => selectedItem(context, 4),
            ),
          ],
        ),
      )),
      appBar: AppBar(
          title: Text("PROCESO"),
          backgroundColor: Colors.blueAccent,
          actions: <Widget>[
            IconButton(
                icon: const Icon(
                  Icons.account_circle_rounded,
                  color: Colors.white,
                  size: 28.0,
                ),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => const userProfile()));
                  // do something
                  //leading: IconButton(
                  //icon: const Icon(Icons.account_circle_rounded, size: 50.0,), onPressed: () {  },
                  //actions: [Icon(Icons.account_circle_rounded)],
                })
          ]),
      body: IndexedStack(
        index: _currentIndex,
        children: screens,
      ),
      //screens[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          type: BottomNavigationBarType.fixed,
          backgroundColor: Colors.lightBlue,
          selectedItemColor: Colors.white,
          iconSize: 22,
          items: const [
            BottomNavigationBarItem(
                icon: Icon(Icons.calendar_today),
                label: "Calendar",
                backgroundColor: Colors.black12),
            BottomNavigationBarItem(
                icon: Icon(Icons.home),
                label: "Home",
                backgroundColor: Colors.black12),
            BottomNavigationBarItem(
                icon: Icon(Icons.circle_notifications_rounded),
                label: "Notifications",
                backgroundColor: Colors.black12),
          ],
          onTap: (index) {
            setState(() {
              _currentIndex = index;
            });
          }),
    );
  }

  Widget buildMenuItem({
    VoidCallback? onClicked,
    required String text,
    required IconData icon, // Function() onClicked,
  }) {
    final color = Colors.white;

    return ListTile(
      leading: Icon(icon, color: color),
      title: Text(text, style: TextStyle(color: color)),
      onTap: onClicked,
      //() {
      //Navigator.of(context)
      // .push(MaterialPageRoute(builder: (context) => Calendar()));
      //},
    );
  }

  void selectedItem(BuildContext context, int index) {
    Navigator.of(context).pop();
    switch (index) {
      case 0:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => home(),
        ));
        break;

      case 1:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => processes(),
        ));
        break;

      case 2:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => mindMapping(),
        ));
        break;

      case 3:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => tasks(),
        ));
        break;
      case 4:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => userStories(),
        ));
        break;
    }
  }
}
